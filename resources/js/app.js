// window.Vue = require('vue');
import Vue from 'vue';
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import routes from './router/index';

/**
     The following block of code may be used to automatically register your
     Vue components. It will recursively scan this directory for the Vue
     components and automatically register them with their "basename".
	
     Eg. ./components/ExampleComponent.vue -> <example-component></example-component>

     const files = require.context('./', true, /\.vue$/i)
     files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

     Vue.component('example-component', require('./components/ExampleComponent.vue').default);

     * Next, we will create a fresh Vue application instance and attach it to
     * the page. Then, you may begin adding components to this application
     * or customize the JavaScript scaffolding to fit your unique needs.
**/

import { HasError, AlertError } from 'vform';

import store from './store/index';

// https://github.com/chengxulvtu/cxlt-vue2-toastr
// success/info/warn/error/removeAll
import CxltToastr from 'cxlt-vue2-toastr';
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';
var toastrConfigs = {
    position: 'top right',
    // showDuration: 1000,
    // timeOut: 8000,
    // closeButton: true,
    // showMethod: 'fadeIn',
    // hideMethod: 'fadeOut',

    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    // "onclick": null,
    "clickToClose": true,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

Vue.use(CxltToastr, toastrConfigs);

$(function () {
    $(document).on('click', '.cxlt-toastr-container', function (index2, el2) {

        $(document).find('.cxlt-toastr-container').remove();
    });
});

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

Vue.component('app-header', require('./components/Header.vue').default);

// Vue router navigation
function isLoggedIn() {
    return store.getters.getAuthenticated;
}

routes.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!isLoggedIn()) {
            next({
                name: 'login',
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (isLoggedIn()) {
            next({
                name: 'dashboard',
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
});

let auth = localStorage.getItem('auth');

if (auth) {

    store.dispatch('authUser').then(() => {
        const app = new Vue({
            el: '#app',
            router: routes,
            store,
        });
    });

} else {

    const app = new Vue({
        el: '#app',
        router: routes,
        store,
    });
}
