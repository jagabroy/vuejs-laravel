import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../pages/home.vue';

import CategoryList from '../pages/category/index.vue';
import CategoryCreate from '../pages/category/create.vue';
import EditCategory from '../pages/category/edit.vue';
import ProductList from '../pages/product/index.vue';
import CreateProduct from '../pages/product/create.vue';
import EditProduct from '../pages/product/edit.vue';
import Dashboard from '../pages/dashboard/index.vue';

// Authentication Files
import Login from '../pages/auth/Login.vue';

const routes = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: Home,
			name: 'home',
		},
		{
			path: '/category',
			component: CategoryList,
			name: 'category-list',
		},
		{
			path: '/category/create',
			component: CategoryCreate,
			name: 'category-create',
		},
		{
			path: '/category/edit/:id',
			component: EditCategory,
			name: 'category-edit',
		},
		{
			path: '/product',
			component: ProductList,
			name: 'product-list',
			meta: {
				auth: true
			}
		},
		{
			path: '/product/create',
			component: CreateProduct,
			name: 'product-create',
		},
		{
			path: '/product/edit/:id',
			component: EditProduct,
			name: 'product-edit',
		},
		{
			path: '/auth/login',
			component: Login,
			name: 'login',
			meta: {
				requiresVisitor: true
			}
		},
		{
			path: '/dashboard',
			component: Dashboard,
			name: 'dashboard',
			meta: {
				requiresAuth: true
			}
		},



	]
});

export default routes;